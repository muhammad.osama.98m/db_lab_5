# DB_Lab_5



## Assumptions
- Some hotels were not in the dataset so the queries related to them would return null or empty tables.
- null value for dateto is counted as numerically equal to current date or greater than it.

## TODO
- Add the insert statements and schema definition.
- Optimize slower queries.
- Add in line results in the SQL query file.
